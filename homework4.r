---
title: "Homework 4"
output: html_notebook
---

```{r Question 1}
#
x = read.csv("C:\\Users\\kicky\\Downloads\\dat1(2).csv")
x = as.matrix(x[2:3])
x
sample = x[,2]
male = x[1:23,2]
typeof(male)
female = x[24:50,2]
sample = as.double(sample)
male = as.double(male)
male
female = as.double(female)
female
```
We hypothesise H0: mu1 = mu2 vs. Ha: !H0
```{r Question 1}
x1bar = mean(male)
x2bar = mean(female)
xbar = mean(sample)
n1 = length(male)
n2 = length(female)
sstr = (n1*((x1bar-xbar)^2)) + (n2*((x2bar-xbar)^2))

ssres = 0
for(i in 1:n1){
  ssres = ssres + ((male[i]-x1bar)^2)
}
for(i in 1:n2){
  ssres = ssres + ((female[i]-x2bar)^2)
}
ssmean = (n1+n2)*xbar^2
male^2 == (male*male)
ssobs = sum(male*male)+ sum(female*female)
ssobs == (ssmean+sstr+ssres)
f_stat = (sstr/(ssres/(n1+n2-2)))
alpha = 0.05
critical_value = qf(1-alpha,n-1,n1+n2-2)
result=c("Do not reject H0")
if(f_stat > critical_value){
  result = c("Reject H0")
}
print("ssres = ")
print(ssres)
print("sstr = ")
print(sstr)
print("ssobs = ")
print(ssobs)
print("fstat = ")
print(fstat)
result
```

1b.
```{r setup}
x = read.csv("C:\\Users\\kicky\\Downloads\\dat1(2).csv")
dummies <- as.numeric(x[,2] == 'M')
ratings <- as.numeric(x[,3])
ratings
xwithdummies = cbind(dummies,ratings)
xwithdummies
x1 = xwithdummies[,1]
x1
y = xwithdummies[,2]
y
ybar = mean(y)
ybar
xbar = mean(x1)
xbar

beta1 = 0
n = length(x1)
for (i in 1:n){
  beta1 = beta1 + (y[i]-ybar)*(x1[i]-xbar)
}
beta1
denom = 0
for (i in 1:n){
  denom = denom + (x1[i]-xbar)^2
}
b1 = beta1/denom

b0 = ybar-b1*xbar
bhat = c(b0,b1)
bhat
s2 = (t(y-x1*bhat)%*%(y-x1*bhat))/(47)
s2
constant = rep(1,n)
full = cbind(constant,dummies)
females = rep(0,n/2)
constant = rep(1,n/2)
reduced = cbind(constant,females)
reduced
yf = head(y,25)

ssresl = t(yf - reduced%*%bhat)%*%(yf-reduced%*%bhat)
ssresx = t(y - full%*%bhat)%*%(y-full%*%bhat)
ssresl
ssresx
test_statistic = (ssresl -ssresx)/s2
test_statistic
criticalvalue = qf(0.95, 2,47)
criticalvalue
#beta = solve(t(x1)%*%x1)%*%(t(x1)%*%y)
#beta
```

```{r Question 1b}
x = female
y = male
xbar = mean(female)
ybar = mean(male)
n = n1
if(n2<n1){
  n=n2
}

b1 = 0
for(i in 1:n){
  b1 = b1 + (y[i]-ybar)*(x[i]-xbar)
}
denom = 0
for (i in 1:n){
  denom = denom + (x[i]-xbar)^2
}
b1 = b1/denom
b1
b0 = ybar - b1*xbar
b0
yplot = c()
for (i in 1:n){
  yplot[i] = b0+b1*(i)
}
yplot
xplot = 1:n
xplot
plot(x[1:n],y[1:n], main="Plot")
abline(a=xplot,b=yplot,col="red")

```


```{r Question 2}
y = c(15,9,3,25,7,13)
ybar = mean(y)
x = cbind(c(1,1,1,1,1,1),c(10,5,7,19,11,8))
x
beta = solve(t(x)%*%x)%*%(t(x)%*%y)
yhat = x%*%beta
eps = y-yhat
eps
t(eps)%*%(eps)
sum((eps*eps))
sst = sum(t(y-ybar)%*%(y-ybar))
sst
ssr = sum(t(yhat-ybar)%*%(yhat-ybar))
ssr
sse = sum(t(eps)%*%(eps))
sse

r2 = 1-sse/sst
r2
(t(eps)%*%eps)
s2 = (1/4)*(t(eps)%*%eps)
s2
solve(t(x)%*%x)
s2 = 25.36667
teststat = t(beta)%*%(s2*(solve(t(x)%*%x)))%*%beta
teststat
criticalvalue = 2*qf(0.95,2,4)
criticalvalue
if (teststat>criticalvalue){
  print("bitch reject")
}
```